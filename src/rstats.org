#+title: R and Statistics
#+subtitle: Statistical Analysis with R Language
#+author: Alberto Valdez
#+SETUPFILE: ../config/org-theme-alt.config
#+SETUPFILE: ../config/org-header.config
#+PROPERTY: header-args:R :session "inferior ESS"

* Introduction to R
:PROPERTIES:
:CUSTOM_ID: introduction-to-r
:END:

R is a programming language used for statistics, data analysis and machine learning which is very popular in academia.

It can be used for many other applications in different domains. It provides the basic tools we need to do computational statistics. It is also very extensible with objective-specific libraries, for example domain specific libraries for pharmaceutic statistic, production-optimized research, etc.

- It is not as easy to create an automated process as it is with Python.
- It is fast and light.
- Specializes packages.
- Great visualization libraries.

R is normally used within the R Studio app[fn:1].

#+begin_src shell
R --version
#+end_src

#+RESULTS[c431766663899d1668813b8f535eb502589140d8]:
#+begin_example
R version 4.2.1 (2022-06-23) -- "Funny-Looking Kid"
Copyright (C) 2022 The R Foundation for Statistical Computing
Platform: x86_64-apple-darwin17.0 (64-bit)

R is free software and comes with ABSOLUTELY NO WARRANTY.
You are welcome to redistribute it under the terms of the
GNU General Public License versions 2 or 3.
For more information about these matters see
https://www.gnu.org/licenses/.

#+end_example

** Environment and Packages
:PROPERTIES:
:CUSTOM_ID: environment-and-packages
:END:

R can be used via command line, scripts or R studio[fn:2].

We can install packages within R Studio using =install.packages=.

#+begin_src R :eval no
install.packages("tidyverse")
install.packages("jsonlite")
#+end_src

We can also use R Notebooks[fn:3] for writing code.

In R Studio, we can run specific lines by selecting a line and pressing =ctrl+Enter= or all the script by not selecting anything.

* Syntax
:PROPERTIES:
:CUSTOM_ID: syntax
:END:
** Variables
:PROPERTIES:
:CUSTOM_ID: variables
:END:

#+begin_src R
x <- 'hello world'
x
check <- TRUE
check
c <- F
c
num <- 100
num
#+end_src

#+RESULTS[695d057099f28264c19a86b4c04cc30bc91b6e4e]:
#+begin_example
[1] "hello world"
[1] TRUE
[1] FALSE
[1] 100
#+end_example

There are no types and we can override types as we go.

#+begin_src R
x <- "abc"
x
x <- 100
x
#+end_src

#+RESULTS[45438cc51490695a73a4868657d1985ae4e7df24]:
#+begin_example
[1] "abc"
[1] 100
#+end_example

The =c= function is short for =concatenate=. We can use it for combining individual values into a =vector=.

#+begin_src R
numlist <- c(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
numlist
#+end_src

#+RESULTS[4747b512dc25cc7c6c53f2ed35f5313c093b2159]:
#+begin_example
 [1] 0 1 2 3 4 5 6 7 8 9
#+end_example

All those variables are stored in the envrionment.

#+begin_src R
vars <- Sys.getenv(x = NULL, unset = "", names = NA)
head(vars, 2)
#+end_src

#+RESULTS[d6bd7de93a617317619615a06e7fa4724e223d23]:
#+begin_example
COLUMNS                 126
DICTIONARY              en_US
#+end_example

** Data Structures
:PROPERTIES:
:CUSTOM_ID: data-structures
:END:

- A numeric vector is an ordered list of numbers. All values must be of the same type.

*NOTE* The first index in a vector is =1=.
#+begin_src R
v <- c(10, 20, 30, 40, 50)
v
v[1]
#+end_src

#+RESULTS[19c58f518ff09067b5d6ea45ba6bee04b9be8614]:
#+begin_example
[1] 10 20 30 40 50
[1] 10
#+end_example

- A matrix can be thought of as a vector of vectors, where each value in the matrix is the same data type.

#+begin_src R
matrix(1:9, nrow = 3, ncol = 3)
#+end_src

#+RESULTS[28309307554a141a6eb1e992a60661a01827802b]:
#+begin_example
     [,1] [,2] [,3]
[1,]    1    4    7
[2,]    2    5    8
[3,]    3    6    9
#+end_example

- A data frame is a collection with columns with different data types.

#+begin_src R
Name <- c("Jon", "Bill", "Maria", "Ben", "Tina")
Age <- c(23, 41, 32, 58, 26)
df <- data.frame(Name, Age)
df
#+end_src

#+RESULTS[9a9dcf181c1c799037c3cab50ff2671dfbb54621]:
#+begin_example
   Name Age
1   Jon  23
2  Bill  41
3 Maria  32
4   Ben  58
5  Tina  26
#+end_example

- A tibble is introduced by the tidyverse package in R and is an optimized data frame with extra metadata and features.

** Functions
:PROPERTIES:
:CUSTOM_ID: functions
:END:

Functions have name, arguments, body and return.

#+begin_src R
fahrenheit_to_celsius <- function(temp_F) {
  temp_C <- (temp_F - 32) * 5 / 9
  return(temp_C)
}
fahrenheit_to_celsius(77)
#+end_src

#+RESULTS[dd9fc9053c4d1cbfcb6bca97672b600499216574]:
#+begin_example
[1] 25
#+end_example

We can get information on the function using =help()=.

#+begin_src R :eval no
help(read.csv)
#+end_src

** Loops and Logic
:PROPERTIES:
:CUSTOM_ID: loops-and-logic
:END:

#+begin_src R
a <- c(0, 1, 2)
b <- c(3, 4, 5)
ab <- c(a, b)
for (x in ab){
  print(x)
}
#+end_src

#+RESULTS[7bf03a583b3fffdf1ab3394eaf216e6095d5f595]:
#+begin_example
[1] 0
[1] 1
[1] 2
[1] 3
[1] 4
[1] 5
#+end_example

We can use conditional statements and combine them with =next= for skipping values in a loop.

#+begin_src R
data <- c('Bob', 'Alice', 'Jonathan', 'Alex')
for (x in data){
  if (nchar(x) < 5) {
    next
  }
  else {
    print(x)
  }
}
#+end_src

#+RESULTS[3ca97119952dfa9e58278ac8fdf8aa3ca8817f5f]:
#+begin_example
[1] "Alice"
[1] "Jonathan"
#+end_example

We can't use =length= on a string to get its length because it is designed to work with length of vectors. In R, strings are more similar to a character, so we must use =nchar= instead.

** Lists
:PROPERTIES:
:CUSTOM_ID: lists
:END:

Lists can contain different data types. A list in R is similar to a dictionary in Python (key, value pair).

#+begin_src R
movies <- list(
  "movies"= c("Star Wars", "Avatar"),
  "directors"= c("George Lucas", "James Cameron"),
  "rating"= c(4, 5),
  "good"= c(T, T))
movies
#+end_src

#+RESULTS[6d9892931d6b4063a530b24adcfd27dc64e0188e]:
#+begin_example
$movies
[1] "Star Wars" "Avatar"

$directors
[1] "George Lucas"  "James Cameron"

$rating
[1] 4 5

$good
[1] TRUE TRUE
#+end_example

We can use key notation for getting data from lists. The dollar =$= notation allows us to pull the values from a key.

#+begin_src R
movies['rating']
movies$directors
typeof(movies$good)
#+end_src

#+RESULTS[e7cc1fe6c017fda8f1f21e80aa820f7ebd489659]:
#+begin_example
$rating
[1] 4 5
[1] "George Lucas"  "James Cameron"
[1] "logical"
#+end_example

** Code Example
:PROPERTIES:
:CUSTOM_ID: code-example
:END:

#+begin_src R :results silent
library(tidyverse)
#+end_src

#+begin_src R :tangle ../resources/01/02/jr_high.R
# Part I
students <- c("Abraham", "Beatrice", "Cory", "Dinah", "Eric", "Felicia")

roll_call <- function(class){
  print(Sys.Date())
  for (student in class){
    print(student)
  }
}
roll_call(students)
# Part II
locker_combinations <- function(class){
  # Create the for loop and print the student name and locker combination.
  for (student in class){
    locker <- toString(sample(33, 3))
    print(str_interp("${student}: ${locker}"))
  }
}
# Call the function with the student vector as an argument.
locker_combinations(students)

# Part III
print("SECURITY BREACH. Changing locker nums.")
for (student in students){
  # Create a variable (substring)  that holds the second letter for each student.
  # Create an if statement to find the names of students where the
  # second letter that is an "e".
  substring <- substr(student, 2, 2)
  if (substring == 'e'){
    locker <- toString(sample(33:66, 3))
    print(str_interp("${student}: ${locker}"))
  }
}
#+end_src

#+RESULTS[4d1751437bda28488c1b75e948d7aabaf4cf37d5]:
#+begin_example
[1] "2022-10-04"
[1] "Abraham"
[1] "Beatrice"
[1] "Cory"
[1] "Dinah"
[1] "Eric"
[1] "Felicia"
[1] "Abraham: 8, 21, 1"
[1] "Beatrice: 26, 13, 24"
[1] "Cory: 28, 20, 9"
[1] "Dinah: 3, 8, 19"
[1] "Eric: 3, 16, 6"
[1] "Felicia: 14, 20, 8"
[1] "SECURITY BREACH. Changing locker nums."
[1] "Beatrice: 57, 50, 38"
[1] "Felicia: 43, 37, 65"
#+end_example

* Vectors
:PROPERTIES:
:CUSTOM_ID: vectors
:END:

** Vector Operations
:PROPERTIES:
:CUSTOM_ID: vector-operations
:END:

The colon operator lets us create a vector from a range.

#+begin_src R
v <- 1:5
v
v <- 1:length(ab)
#+end_src

#+RESULTS[37f620f774515cbba59a301ca2c492490ad96315]:
#+begin_example
[1] 1 2 3 4 5
#+end_example

Then we can loop across many vectors using the =i= iterator from the =length= function.

#+begin_src R
for (i in 1:length(ab)){
  print(ab[i] + v[i])
}
#+end_src

#+RESULTS[82ba73b89dd2f182dda5ccf15821f8c8a6e228b7]:
#+begin_example
[1] 1
[1] 3
[1] 5
[1] 7
[1] 9
[1] 11
#+end_example

We can do vector arithmetic too.

#+begin_src R
v2 <- v ** 2
v2
#+end_src

#+RESULTS[b74ff0872d8eecb81e39f700f3a6b49f52b24846]:
#+begin_example
[1]  1  4  9 16 25 36
#+end_example

** Vectors Name
:PROPERTIES:
:CUSTOM_ID: vectors-name
:END:

We can use =name= to combine two arrays into a key, value table. Which we can access as either key or index and the result is the key, value pair.

#+begin_src R :results silent
library(dplyr)
#+end_src

#+begin_src R
months <- c("January", "February", "March", "April")
precipitation <- c(0.3, 0.4, 0.6, 0.4)
names(precipitation) <- months
precipitation
precipitation["February"]
precipitation[2]
#+end_src

#+RESULTS[3ace197b48259dfedc5cd1b7c11e88cc5b3e6142]:
#+begin_example
 January February    March    April
     0.3      0.4      0.6      0.4
February
     0.4
February
     0.4
#+end_example

Using =dplyr= summary function.

#+begin_src R
p_sum <- summary(precipitation)
p_sum
p_sum["Min."]
p_sum["Mean"]
p_sum[["Mean"]]
#+end_src

#+RESULTS[3d12f1d86fa0dafe19dec3158706c99c4acaa90b]:
#+begin_example
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max.
  0.300   0.375   0.400   0.425   0.450   0.600
Min.
 0.3
 Mean
0.425
[1] 0.425
#+end_example

** Pipe Operator
:PROPERTIES:
:CUSTOM_ID: pipe-operator
:END:

The pipe operator =%>%= composes functions, which allows us to concatenate a sequence of operations.

#+begin_src R
precipitation %>% summary
#+end_src

#+RESULTS[510529a95c6e4bac230856568061cac5d473ce29]:
#+begin_example
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max.
  0.300   0.375   0.400   0.425   0.450   0.600
#+end_example

For comparison.
#+begin_src R :eval no
F(G(H(P))) == P %>% H %>% G %>% F
#+end_src

More examples.
#+begin_src R
precipitation %>% summary %>% length
precipitation %>% sd %>% round(2)
precipitation %>% sum
#+end_src

#+RESULTS[d057d46eaca0939c0879405f0895ecd5d236c76e]:
#+begin_example
[1] 6
[1] 0.13
[1] 1.7
#+end_example


* Tibbles
:PROPERTIES:
:CUSTOM_ID: tibbles
:END:

** Tibble Basics
:PROPERTIES:
:CUSTOM_ID: tibble-basics
:END:

Tibbles are enabled by the =tidyverse= library. They are similar to data frames but more sophisticated.

#+begin_src R :results silent
library(tidyverse)
#+end_src

We will import sample data from a package. It will return a tibble data type.

#+begin_src R :results value table :colnames yes :wrap org
data(diamonds, package='ggplot2')
head(diamonds, 3)
#+end_src

#+RESULTS[b5516ecc3f602f798c393740aed3a948c141c564]:
#+begin_org
| carat | cut     | color | clarity | depth | table | price |    x |    y |    z |
|-------+---------+-------+---------+-------+-------+-------+------+------+------|
|  0.23 | Ideal   | E     | SI2     |  61.5 |    55 |   326 | 3.95 | 3.98 | 2.43 |
|  0.21 | Premium | E     | SI1     |  59.8 |    61 |   326 | 3.89 | 3.84 | 2.31 |
|  0.23 | Good    | E     | VS1     |  56.9 |    65 |   327 | 4.05 | 4.07 | 2.31 |
#+end_org

We can select data with =slice=.
#+begin_src R :results value table :colnames yes :wrap org
slice(diamonds, 3)
slice(diamonds, 1:7)
slice(diamonds, c(1, 7))
#+end_src

#+RESULTS[47d8b7ea6ed1c8f07a8c751d0b11700844371df3]:
#+begin_org
| carat | cut       | color | clarity | depth | table | price |    x |    y |    z |
|-------+-----------+-------+---------+-------+-------+-------+------+------+------|
|  0.23 | Ideal     | E     | SI2     |  61.5 |    55 |   326 | 3.95 | 3.98 | 2.43 |
|  0.24 | Very Good | I     | VVS1    |  62.3 |    57 |   336 | 3.95 | 3.98 | 2.47 |
#+end_org

** Filtering Data
:PROPERTIES:
:CUSTOM_ID: filtering-data
:END:

We use =filter= and conditionals for filtering data similar to a dataframe or sql.

#+begin_src R :results value table :colnames yes :wrap org
f1 <- filter(diamonds, cut=='Ideal')
f2 <- filter(diamonds, (cut=='Ideal' & price > 500))
head(f2, 3)
#+end_src

#+RESULTS[ce527cdc12e555f27b7b98f911f433dfac33b02c]:
#+begin_org
| carat | cut   | color | clarity | depth | table | price |    x |    y |    z |
|-------+-------+-------+---------+-------+-------+-------+------+------+------|
|  0.35 | Ideal | I     | VS1     |  60.9 |    57 |   552 | 4.54 | 4.59 | 2.78 |
|   0.3 | Ideal | D     | SI1     |  62.5 |    57 |   552 | 4.29 | 4.32 | 2.69 |
|   0.3 | Ideal | D     | SI1     |  62.1 |    56 |   552 |  4.3 | 4.33 | 2.68 |
#+end_org

#+begin_src R :results value table :colnames yes :wrap org
r1 <- diamonds %>% filter((cut=='Ideal' & price > 500)) %>% select(carat, price)
head(r1, 5)
#+end_src

#+RESULTS[4a3a2190e744ef8d29ba801d5e4b6f39faeaddf6]:
#+begin_org
| carat | price |
|-------+-------|
|  0.35 |   552 |
|   0.3 |   552 |
|   0.3 |   552 |
|  0.28 |   553 |
|  0.32 |   553 |
#+end_org

** Mutate and Summary
:PROPERTIES:
:CUSTOM_ID: mutate-and-summary
:END:

Mutate adds new data to a column, we can name the columns with the variable we use for the operation.

#+begin_src R :results value table :colnames yes :wrap org
totalVol <- mutate(diamonds, totalVolume=(x*y*z))
head(totalVol, 3)
#+end_src

#+RESULTS[16ba7988bce8b0e9be83944a54914362ca16459c]:
#+begin_org
| carat | cut     | color | clarity | depth | table | price |    x |    y |    z | totalVolume |
|-------+---------+-------+---------+-------+-------+-------+------+------+------+-------------|
|  0.23 | Ideal   | E     | SI2     |  61.5 |    55 |   326 | 3.95 | 3.98 | 2.43 |    38.20203 |
|  0.21 | Premium | E     | SI1     |  59.8 |    61 |   326 | 3.89 | 3.84 | 2.31 |   34.505856 |
|  0.23 | Good    | E     | VS1     |  56.9 |    65 |   327 | 4.05 | 4.07 | 2.31 |   38.076885 |
#+end_org

Getting summary values.

#+begin_src R :results value table :colnames yes :wrap org
s <- summarize(diamonds, mean(price))
head(s, 3)
#+end_src

#+RESULTS[b86a2f570c1c5ab6b3f229d2b2876870bb3bace9]:
#+begin_org
|      mean(price) |
|------------------|
| 3932.79972191324 |
#+end_org

** Grouping Data
:PROPERTIES:
:CUSTOM_ID: grouping-data
:END:

Grouping data as summary.

#+begin_src R :results value table :colnames yes :wrap org
cut <- group_by(diamonds, cut)
s <- summarize(cut, mean(price), sum(price))
head(s, 3)
#+end_src

#+RESULTS[3ddb26bc0f29d2f95097dc5d94f9cf16ae6e11d8]:
#+begin_org
| cut       |      mean(price) | sum(price) |
|-----------+------------------+------------|
| Fair      | 4358.75776397516 |    7017600 |
| Good      | 3928.86445169181 |   19275009 |
| Very Good | 3981.75989074657 |   48107623 |
#+end_org

#+begin_src R :results value table :colnames yes :wrap org
s <- summarize(cut, avg=mean(price), num=n())
head(s, 3)
#+end_src

#+RESULTS[f3df6ed51843e10cb5971ffd6fc6b3bf215ace8d]:
#+begin_org
| cut       |              avg |   num |
|-----------+------------------+-------|
| Fair      | 4358.75776397516 |  1610 |
| Good      | 3928.86445169181 |  4906 |
| Very Good | 3981.75989074657 | 12082 |
#+end_org

The summarization of a group by is a tibble.

#+begin_src R :results value table :colnames yes :wrap org
cut2 <- group_by(diamonds, cut, color)
cut2_sum <- summarize(cut2, mean(price))
head(cut2_sum, 3)
#+end_src

#+RESULTS[739fedaed92c239712329c3798a951629d6ddaa0]:
#+begin_org
| cut  | color |      mean(price) |
|------+-------+------------------|
| Fair | D     | 4291.06134969325 |
| Fair | E     |        3682.3125 |
| Fair | F     | 3827.00320512821 |
#+end_org

We can use the group by as a tibble.

#+begin_src R :results value table :colnames yes :wrap org
c <- count(diamonds, cut)
head(c, 3)
#+end_src

#+RESULTS[0f5a018837e2a738bdb9b936bf845c9026213939]:
#+begin_org
| cut       |     n |
|-----------+-------|
| Fair      |  1610 |
| Good      |  4906 |
| Very Good | 12082 |
#+end_org


* Reading Files with R
:PROPERTIES:
:CUSTOM_ID: reading-files-with-r
:END:

** Working with diectories
:PROPERTIES:
:CUSTOM_ID: working-with-diectories
:END:

We can get the working directory, change it, list files and create files.
#+begin_src R
getwd()
list.files()
setwd("src")
list.files()
setwd("..")
file.exists("README.md")
file.create("deleteme.R")
file.info("deleteme.R")
file.remove("deleteme.R")
#+end_src

#+RESULTS[4f96530f9877ec50ec6a6bf8d1416fdf008e8313]:
#+begin_example
[1] "/Users/albertovaldez/data-notes/resources/week-15"
[1] "LICENSE"   "Makefile"  "README.md" "config"    "docs"      "public"
[7] "resources" "src"       "tests"
[1] "rstats.org"
[1] TRUE
[1] TRUE
           size isdir mode               mtime               ctime
deleteme.R    0 FALSE  644 2022-10-06 20:39:03 2022-10-06 20:39:03
                         atime uid gid         uname grname
deleteme.R 2022-10-06 20:39:03 501  20 albertovaldez  staff
[1] TRUE
#+end_example

** Reading CSV files
:PROPERTIES:
:CUSTOM_ID: reading-csv-files
:END:

#+begin_src R :results value table :wrap org
demo_table <- read.csv(file='resources/demo.csv',check.names=F,stringsAsFactors = F)
demo_table
#+end_src

#+RESULTS[fcbe601357302cbbfe283e0f1f485850c89eaca1]:
#+begin_org
| John   | Compact_Sedan    | 2012 | 87456 |
| Claire | Pickup           | 2017 | 15022 |
| Xavier | SUV              | 2019 |  4532 |
| Kerri  | Subcompact_Sedan | 2018 | 12349 |
#+end_org

** Reading JSON files
:PROPERTIES:
:CUSTOM_ID: reading-json-files
:END:

#+begin_src R :results silent
library(jsonlite)
#+end_src

#+begin_src R :eval no
demo_table2 <- fromJSON(txt='resources/demo.json')
head(demo_table2, 2)
#+end_src


* Hypothesis Testing
:PROPERTIES:
:CUSTOM_ID: hypothesis-testing
:END:

** Exploratory Data Analysis
:PROPERTIES:
:CUSTOM_ID: exploratory-data-analysis
:END:

In data science we are constantly manipulating, visualizng and summarizing data. This process is known as *exploratory data analysis*, which is part of the scientific method. It tells us how the data behaves, what happens in the past and what is happening right now. The complete scientific process would look like this:

Exploratory Analysis -> Hypothesis -> Experiment -> Hypothesis Testing -> Hypothesis Supported | Hypothesis Not Supported -> Report Results

** Hypothesis
:PROPERTIES:
:CUSTOM_ID: hypothesis
:END:

A hypothesis is a proposed solution or answer to a question that we ask the data. We must ask specific questions that relate to the data contents and we know the data is be able to answer.

For example: Does this new medication help patients recover faster compared to a placebo group?

The hypothesis must have a specific structure so we can build an analysis around it. The outcome is something we can measure and compare, in this case recovery time over placebo patients.

*IF* we give patients the new drug -> *THEN* -> they recover 50% faster.

More examples:

*Question*: Is the cost of living in my city higher than another one?

*Hypothesis*: If we collect metrics around cots of living and total them together, is the average cost of living in my city is higher than the other city? In what percentage?

*Question*: Can we predict a IMDB score based in the score of the movie?

*Hypothesis*: Is there a linear relationship between the length of the movie and the IMDB score, if there is, the slope of the line should be nonzero.

The more specific, the easier the Hypothesis it is to test. However, the more specific a question is, the less meaningful it becomes, so we need to find a balance between generalization and testeability.

** Testing the Hypothesis
:PROPERTIES:
:CUSTOM_ID: testing-the-hypothesis
:END:

Once we have collected or generated the data, we can test it with a given expectation.

Null Hypothesis typically states that there is no difference between the variables of interests. There is nothing to look at the data, everything is the same.

"Having this observation doesn't mean that another one will be related".

Alternative Hypothesis is what we will reflect our guess upon, which is closer to our actual Hypothesis. It is a way to negate the Null Hyphotesis.

"If we have this observation, then this should be true".

Our test consists on negating the Null Hypothesis, that it can be rejected. However, we will never reject the Null Hypothesis absolutely.

The =p-value= (probability value) is the chance of the Null Hypothesis to occurr. Having a result at least as extreme as what was previously observed.

If we have a low enough =p-value=, we can reject the Null Hypothesis. The =confidence= is the threshold that we can accept for not negating the Null Hypothesis.

The significance of the test is the probability that we are wrong. The confidence limit is normally =95%=, so we will aim for significances of 5%, 2.5% or 1%. If the =p-value= is lower that the significance, we can reject the Null Hypothesis.

1. Determine the Hypothesis and Null Hypothesis.
2. Identify appropiate statistical test.
3. Determine acceptable significance value.
4. Compute p-value.
5. Determine if p-value rejects the null hypothesis.

** Types of Tests
:PROPERTIES:
:CUSTOM_ID: types-of-tests
:END:

The t-test is a way to compare the mean of a sample against a population or another sample. The way we compute one-sample t-test is different from two-sample t-test.

We are able to compare a sample to a population taken from the same sample. The population's metrics are computed differently to samples metrics, as the later are estimated metrics.

The Null Hypothesis assumes that there are no meaningful differences between the two means. The goal of the t-test is to reject the Null Hypothesis.

A Null Hypothesis would be "if we compare the mean of a sample to the population's mean, there would be no difference", so we use the t-test for this case.

The result of a t-test is a test statistic. We can convert the test result to a p-value by building a member of the distribution and we will measure the probability of that measure to happen, which is the p-value.

For example: we have a medication we are testing, and we are comparing it to another product, where our hypothesis is that it is more effective than the other product.

* Hypothesis Testing in R
:PROPERTIES:
:CUSTOM_ID: hypothesis-testing-in-r
:END:

** One-Sample T-Test
:PROPERTIES:
:CUSTOM_ID: one-sample-t-test
:END:

We can construct a =t-test= in R by creating a sample of the population and then using =t.test= to compare them.

In the first case, the =p-value= is very high so we cannot reject the Null Hypothesis.

#+begin_src R
set.seed(42)
population1 = rnorm(1000)
population2 = sample(population1, 200)
t.test(population2, mu=mean(population1))
#+end_src

#+RESULTS[6f5d16687510e8679aa74df61c36390c87520ed5]:
#+begin_example

	One Sample t-test

data:  population2
t = 0.67172, df = 199, p-value = 0.5025
alternative hypothesis: true mean is not equal to -0.02582443
95 percent confidence interval:
 -0.1222976  0.1703285
sample estimates:
 mean of x
0.02401546
#+end_example

In this case, with a different population, the =p-value= is very low, so we can reject the Null Hypothesis.

#+begin_src R
population3 = rnorm(1000, -2)
t.test(population2, mu=mean(population3))
#+end_src

#+RESULTS[a0ae45d7d5200a98c55094e8900ff01fc863edcd]:
#+begin_example

	One Sample t-test

data:  population2
t = 27.231, df = 199, p-value < 2.2e-16
alternative hypothesis: true mean is not equal to -1.996426
95 percent confidence interval:
 -0.1222976  0.1703285
sample estimates:
 mean of x
0.02401546
#+end_example

** Two-Sample T-Test
:PROPERTIES:
:CUSTOM_ID: two-sample-t-test
:END:

In our first test, we are comparing two samples, so the test is applied differently.
#+begin_src R
set.seed(42)
population1 = rnorm(1000)
population2 = rnorm(1000)
t.test(population1, population2)
#+end_src

#+RESULTS[8c421eeb602142f401e9da37c7667cfb84efe674]:
#+begin_example

	Welch Two Sample t-test

data:  population1 and population2
t = -0.46115, df = 1997.5, p-value = 0.6447
alternative hypothesis: true difference in means is not equal to 0
95 percent confidence interval:
 -0.10771412  0.06670125
sample estimates:
   mean of x    mean of y
-0.025824427 -0.005317994
#+end_example

In the second case, we are comparing a new sample to our original sample and the result is a very low =p-value=.
#+begin_src R
population1 = rnorm(1000)
population2 = rnorm(1000, -2)
t.test(population1, population2)
#+end_src

#+RESULTS[18a5d67ec2677855f3c0f5bd6f2f8144caacfbd3]:
#+begin_example

	Welch Two Sample t-test

data:  population1 and population2
t = 44.698, df = 1994.9, p-value < 2.2e-16
alternative hypothesis: true difference in means is not equal to 0
95 percent confidence interval:
 1.930014 2.107146
sample estimates:
   mean of x    mean of y
-0.003206987 -2.021786821
#+end_example


** T.Test Example in R
:PROPERTIES:
:CUSTOM_ID: t-test-example-in-r
:END:

#+begin_src shell
cat ../resources/03/Resources/description.md
#+end_src

#+RESULTS[f7198156e3589b539af872b9fe23952f55dcfbae]:
#+begin_example
# Dataset: sardine.dat

Source: F.N. Clark (1936). "Variations in the Number of Vertebrae in Sardine,
Sardinops caerulea (Girard)", Copeia, Vol. 1936, #3, pp.147-150.

Description: Number of Vertebrae and Location of 12858 adult sardines caught
in the Pacific.

Locations:
1=Alaska
2=British Columbia
3=San Francisco
4=Monterey
5=San Pedro
6=San Diego

Variables/Columns
Location   8
Number of Vertebrae   15-16
#+end_example

#+begin_src R :exports none
library(dplyr)
#+end_src

#+RESULTS[b1558e88b0938fce147322bf18c9534134a8b75c]:
#+begin_example
#+end_example

#+begin_src R
sardines <- read.csv(file="./resources/03/Resources/sardines.csv")
# Calculate the population mean for Sardine Vertebrae in Alaska.
# Hint: use the subset() function to get only the data for Alaska.
alaska <- sardines %>% subset(location == 1)
mean(alaska[['vertebrae']])
# Calculate the population mean for Sardine Vertebrae in San Diego.
# Hint: use the subset() function to get only the data for San Diego.
sandiego <- sardines %>% subset(location == 6)
mean(sandiego[['vertebrae']])
# Calculate Independent (Two Sample) T-Test
t.test(alaska[['vertebrae']], sandiego[['vertebrae']])
#+end_src

#+RESULTS[796ccae2b9bc2583e95e7803d2a3a8e7694449bf]:
#+begin_example
[1] 51.5
[1] 51.74336

	Welch Two Sample t-test

data:  alaska[["vertebrae"]] and sandiego[["vertebrae"]]
t = -1.9607, df = 25.984, p-value = 0.06072
alternative hypothesis: true difference in means is not equal to 0
95 percent confidence interval:
 -0.4985070  0.0117813
sample estimates:
mean of x mean of y
 51.50000  51.74336
#+end_example

* ANOVA Test
:PROPERTIES:
:CUSTOM_ID: anova-test
:END:

If we want to compare multiple samples and multiple characteristics, we can use Analysis of Variance (ANOVA). It compares more than two sample groups.

The Alternate Hypothesis is that at least the mean of one population is different, where the Null Hypothesis says that there is no difference between any of the means.

ANOVA compares the means across all samples and determines wether there is a significant difference in at least one sample.

It assumes that the standard deviation is the same among all groups, which means that all samples come from the same population. If that's not true, the result may we wrong.

** ANOVA Test in R
:PROPERTIES:
:CUSTOM_ID: anova-test-in-r
:END:

#+begin_src R :exports none
library(tidyverse)
#+end_src

#+begin_src shell
cat ../resources/05/Resources/description.md
#+end_src

#+RESULTS[b79b88fbc9b7293c7f317cbcc9e367e2d9854475]:
#+begin_example
# Description

Studies conducted at the University of Melbourne indicate that there may be a difference between the pain thresholds of blonds and brunettes. Men and women of various ages were divided into four categories according to hair colour: light blond, dark blond, light brunette, and dark brunette. The purpose of the experiment was to determine whether hair colour is related to the amount of pain produced by common types of mishaps and assorted types of trauma. Each person in the experiment was given a pain threshold score based on his or her performance in a pain sensitivity test (the higher the score, the higher the person’s pain tolerance).

Variable      Values
HairColour    LightBlond, DarkBlond, LightBrunette or DarkBrunette
Pain          Pain threshold score
Download
Data file (tab-delimited text)

Source
Family Weekly, Gainesville, Sun, Gainesville, Florida, February 5, 1978.

McClave, J. T., and Dietrich II, F. H. (1991). Statistics. Dellen Publishing, San Francisco, Exercise 10.20.
Analysis
Pain threshold decreases as hair darkness increases (blonds are tougher!). Light blonds are significantly different from both brunette categories. Other contrasts are not significant.
#+end_example

#+begin_src R :exports code
library(ggplot2)
#+end_src

#+RESULTS[487a5cc49602f50eb9317708517afab77d83ba01]:
#+begin_example
[1] "/Users/albertovaldez/data-notes/resources/week-15/src"
#+end_example

#+begin_src R
hair <- read.csv(file="../resources/05/Resources/hair.csv")
head(hair, 5)
#+end_src

#+RESULTS[437ffc0924a62fdd3eaff649822af8d396e40888]:
#+begin_example
  HairColour Pain
1 LightBlond   62
2 LightBlond   60
3 LightBlond   71
4 LightBlond   55
5 LightBlond   48
#+end_example

#+begin_src R :file ../resources/hair.png :results file graphics :wrap org
#  Plot the data using ggplot
ggplot(hair, aes(x=HairColour, y=Pain)) + geom_boxplot()
#+end_src

#+RESULTS[4df900fa55f84c5daaf9e351af0e9b79f28d4672]:
#+begin_org
[[file:./../resources/hair.png]]
#+end_org

#+begin_src R
# Determine the p-value using ANOVA
summary(aov(Pain ~ HairColour, data=hair))
#+end_src

#+RESULTS[3576b9a81d37d8e3f5722206c7b28e36d1d1b0ee]:
#+begin_example
            Df Sum Sq Mean Sq F value  Pr(>F)
HairColour   3   1361   453.6   6.791 0.00411 **
Residuals   15   1002    66.8
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
#+end_example

* Linear Relationship
:PROPERTIES:
:CUSTOM_ID: linear-relationship
:END:

The equation of a line is =y = ax + b=. Which means we can determine the value of =y= based on =y=.

The =b= will offset the =y= values by a fix amount and the =a= will modify the slope of the curve.

#+begin_src R :results value table :colnames yes :wrap org
diabetes <- read.csv(file="../resources/06/Resources/diabetes.csv")
head(diabetes, 5)
#+end_src

#+RESULTS[7f410f7bdd112f4c6257fb42aa80b59dfb537a80]:
#+begin_org
| X |          age |          sex |          bmi |           bp |           s1 |           s2 |           s3 |           s4 |           s5 |           s6 | One_Year_Disease_Progress |
|---+--------------+--------------+--------------+--------------+--------------+--------------+--------------+--------------+--------------+--------------+---------------------------|
| 0 |  0.038075906 |  0.050680119 |  0.061696207 |  0.021872355 | -0.044223498 | -0.034820763 | -0.043400846 | -0.002592262 |  0.019908421 | -0.017646125 |                       151 |
| 1 | -0.001882017 | -0.044641637 | -0.051474061 | -0.026327835 | -0.008448724 |  -0.01916334 |  0.074411564 | -0.039493383 | -0.068329744 |  -0.09220405 |                        75 |
| 2 |  0.085298906 |  0.050680119 |  0.044451213 | -0.005670611 | -0.045599451 | -0.034194466 | -0.032355932 | -0.002592262 |  0.002863771 | -0.025930339 |                       141 |
| 3 | -0.089062939 | -0.044641637 | -0.011595015 | -0.036656447 |  0.012190569 |  0.024990593 |  -0.03603757 |  0.034308859 |  0.022692023 | -0.009361911 |                       206 |
| 4 |   0.00538306 | -0.044641637 | -0.036384692 |  0.021872355 |  0.003934852 |   0.01559614 |  0.008142084 | -0.002592262 | -0.031991445 | -0.046640874 |                       135 |
#+end_org

Non-clear linear relationship.
#+begin_src R :results file graphics :file ../resources/diabetes.png :wrap org
ggplot(diabetes, aes(bp, One_Year_Disease_Progress)) + geom_point() + geom_smooth(method="lm", se=TRUE)
#+end_src

#+RESULTS[cf340d9def961b0ef86ea57de8835c14ba214235]:
#+begin_org
[[file:./../resources/diabetes.png]]
#+end_org

Building our own linear regretion. The =R-squared= is a coefficient of determination, it tells us which percentage of the variability affects the blood pressure. This gives us a linear relationship between two variables.

#+begin_src R
reg <- lm(One_Year_Disease_Progress ~ bp, data=diabetes)
summary(reg)
#+end_src

#+RESULTS[2c2998dc10546198d587630661f933ad82e3400b]:
#+begin_example

Call:
lm(formula = One_Year_Disease_Progress ~ bp, data = diabetes)

Residuals:
     Min       1Q   Median       3Q      Max
-173.903  -53.122   -7.811   52.057  227.449

Coefficients:
            Estimate Std. Error t value Pr(>|t|)
(Intercept)  152.133      3.294   46.19   <2e-16 ***
bp           714.742     69.252   10.32   <2e-16 ***
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 69.25 on 440 degrees of freedom
Multiple R-squared:  0.1949,	Adjusted R-squared:  0.1931
F-statistic: 106.5 on 1 and 440 DF,  p-value: < 2.2e-16
#+end_example

The number we are interested in is *Adjusted R-squared:  0.1931*.

* Footnotes
:PROPERTIES:
:CUSTOM_ID: footnotes
:END:

[fn:3][[https://uc-r.github.io/r_notebook#:~:text=An%20R%20Notebook%20is%20an,document%20to%20see%20the%20output][R Notebook]]
[fn:2]https://support.rstudio.com/hc/en-us/articles/200711853-Keyboard-Shortcuts
[fn:1]https://www.rstudio.com/
