# Introduction to R

R is a programming language used for statistics, data analysis and machine learning which is very popular in academia.

It can be used for many other applications in different domains. It provides the basic tools we need to do computational statistics. It is also very extensible with objective-specific libraries, for example domain specific libraries for pharmaceutic statistic, production-optimized research, etc.

-   It is not as easy to create an automated process as it is with Python.
-   It is fast and light.
-   Specializes packages.
-   Great visualization libraries.

R is normally used within the R Studio app<sup><a id="fnr.1" class="footref" href="#fn.1" role="doc-backlink">1</a></sup>.

```shell
R --version
```

    R version 4.2.1 (2022-06-23) -- "Funny-Looking Kid"
    Copyright (C) 2022 The R Foundation for Statistical Computing
    Platform: x86_64-apple-darwin17.0 (64-bit)
    
    R is free software and comes with ABSOLUTELY NO WARRANTY.
    You are welcome to redistribute it under the terms of the
    GNU General Public License versions 2 or 3.
    For more information about these matters see
    https://www.gnu.org/licenses/.


## Environment and Packages

R can be used via command line, scripts or R studio<sup><a id="fnr.2" class="footref" href="#fn.2" role="doc-backlink">2</a></sup>.

We can install packages within R Studio using `install.packages`.

```R
install.packages("tidyverse")
install.packages("jsonlite")
```

We can also use R Notebooks<sup><a id="fnr.3" class="footref" href="#fn.3" role="doc-backlink">3</a></sup> for writing code.

In R Studio, we can run specific lines by selecting a line and pressing `ctrl+Enter` or all the script by not selecting anything.


# Syntax


## Variables

```R
x <- 'hello world'
x
check <- TRUE
check
c <- F
c
num <- 100
num
```

    [1] "hello world"
    [1] TRUE
    [1] FALSE
    [1] 100

There are no types and we can override types as we go.

```R
x <- "abc"
x
x <- 100
x
```

    [1] "abc"
    [1] 100

The `c` function is short for `concatenate`. We can use it for combining individual values into a `vector`.

```R
numlist <- c(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
numlist
```

    [1] 0 1 2 3 4 5 6 7 8 9

All those variables are stored in the envrionment.

```R
vars <- Sys.getenv(x = NULL, unset = "", names = NA)
head(vars, 2)
```

    COLUMNS                 126
    DICTIONARY              en_US


## Data Structures

-   A numeric vector is an ordered list of numbers. All values must be of the same type.

**NOTE** The first index in a vector is `1`.

```R
v <- c(10, 20, 30, 40, 50)
v
v[1]
```

    [1] 10 20 30 40 50
    [1] 10

-   A matrix can be thought of as a vector of vectors, where each value in the matrix is the same data type.

```R
matrix(1:9, nrow = 3, ncol = 3)
```

         [,1] [,2] [,3]
    [1,]    1    4    7
    [2,]    2    5    8
    [3,]    3    6    9

-   A data frame is a collection with columns with different data types.

```R
Name <- c("Jon", "Bill", "Maria", "Ben", "Tina")
Age <- c(23, 41, 32, 58, 26)
df <- data.frame(Name, Age)
df
```

       Name Age
    1   Jon  23
    2  Bill  41
    3 Maria  32
    4   Ben  58
    5  Tina  26

-   A tibble is introduced by the tidyverse package in R and is an optimized data frame with extra metadata and features.


## Functions

Functions have name, arguments, body and return.

```R
fahrenheit_to_celsius <- function(temp_F) {
  temp_C <- (temp_F - 32) * 5 / 9
  return(temp_C)
}
fahrenheit_to_celsius(77)
```

    [1] 25

We can get information on the function using `help()`.

```R
help(read.csv)
```


## Loops and Logic

```R
a <- c(0, 1, 2)
b <- c(3, 4, 5)
ab <- c(a, b)
for (x in ab){
  print(x)
}
```

    [1] 0
    [1] 1
    [1] 2
    [1] 3
    [1] 4
    [1] 5

We can use conditional statements and combine them with `next` for skipping values in a loop.

```R
data <- c('Bob', 'Alice', 'Jonathan', 'Alex')
for (x in data){
  if (nchar(x) < 5) {
    next
  }
  else {
    print(x)
  }
}
```

    [1] "Alice"
    [1] "Jonathan"

We can&rsquo;t use `length` on a string to get its length because it is designed to work with length of vectors. In R, strings are more similar to a character, so we must use `nchar` instead.


## Lists

Lists can contain different data types. A list in R is similar to a dictionary in Python (key, value pair).

```R
movies <- list(
  "movies"= c("Star Wars", "Avatar"),
  "directors"= c("George Lucas", "James Cameron"),
  "rating"= c(4, 5),
  "good"= c(T, T))
movies
```

    $movies
    [1] "Star Wars" "Avatar"
    
    $directors
    [1] "George Lucas"  "James Cameron"
    
    $rating
    [1] 4 5
    
    $good
    [1] TRUE TRUE

We can use key notation for getting data from lists. The dollar `$` notation allows us to pull the values from a key.

```R
movies['rating']
movies$directors
typeof(movies$good)
```

    $rating
    [1] 4 5
    [1] "George Lucas"  "James Cameron"
    [1] "logical"


## Code Example

```R
library(tidyverse)
```

```R
# Part I
students <- c("Abraham", "Beatrice", "Cory", "Dinah", "Eric", "Felicia")

roll_call <- function(class){
  print(Sys.Date())
  for (student in class){
    print(student)
  }
}
roll_call(students)
# Part II
locker_combinations <- function(class){
  # Create the for loop and print the student name and locker combination.
  for (student in class){
    locker <- toString(sample(33, 3))
    print(str_interp("${student}: ${locker}"))
  }
}
# Call the function with the student vector as an argument.
locker_combinations(students)

# Part III
print("SECURITY BREACH. Changing locker nums.")
for (student in students){
  # Create a variable (substring)  that holds the second letter for each student.
  # Create an if statement to find the names of students where the
  # second letter that is an "e".
  substring <- substr(student, 2, 2)
  if (substring == 'e'){
    locker <- toString(sample(33:66, 3))
    print(str_interp("${student}: ${locker}"))
  }
}
```

    [1] "2022-10-04"
    [1] "Abraham"
    [1] "Beatrice"
    [1] "Cory"
    [1] "Dinah"
    [1] "Eric"
    [1] "Felicia"
    [1] "Abraham: 8, 21, 1"
    [1] "Beatrice: 26, 13, 24"
    [1] "Cory: 28, 20, 9"
    [1] "Dinah: 3, 8, 19"
    [1] "Eric: 3, 16, 6"
    [1] "Felicia: 14, 20, 8"
    [1] "SECURITY BREACH. Changing locker nums."
    [1] "Beatrice: 57, 50, 38"
    [1] "Felicia: 43, 37, 65"


# Vectors


## Vector Operations

The colon operator lets us create a vector from a range.

```R
v <- 1:5
v
v <- 1:length(ab)
```

    [1] 1 2 3 4 5

Then we can loop across many vectors using the `i` iterator from the `length` function.

```R
for (i in 1:length(ab)){
  print(ab[i] + v[i])
}
```

    [1] 1
    [1] 3
    [1] 5
    [1] 7
    [1] 9
    [1] 11

We can do vector arithmetic too.

```R
v2 <- v ** 2
v2
```

    [1]  1  4  9 16 25 36


## Vectors Name

We can use `name` to combine two arrays into a key, value table. Which we can access as either key or index and the result is the key, value pair.

```R
library(dplyr)
```

```R
months <- c("January", "February", "March", "April")
precipitation <- c(0.3, 0.4, 0.6, 0.4)
names(precipitation) <- months
precipitation
precipitation["February"]
precipitation[2]
```

     January February    March    April
         0.3      0.4      0.6      0.4
    February
         0.4
    February
         0.4

Using `dplyr` summary function.

```R
p_sum <- summary(precipitation)
p_sum
p_sum["Min."]
p_sum["Mean"]
p_sum[["Mean"]]
```

       Min. 1st Qu.  Median    Mean 3rd Qu.    Max.
      0.300   0.375   0.400   0.425   0.450   0.600
    Min.
     0.3
     Mean
    0.425
    [1] 0.425


## Pipe Operator

The pipe operator `%>%` composes functions, which allows us to concatenate a sequence of operations.

```R
precipitation %>% summary
```

     Min. 1st Qu.  Median    Mean 3rd Qu.    Max.
    0.300   0.375   0.400   0.425   0.450   0.600

For comparison.

```R
F(G(H(P))) == P %>% H %>% G %>% F
```

More examples.

```R
precipitation %>% summary %>% length
precipitation %>% sd %>% round(2)
precipitation %>% sum
```

    [1] 6
    [1] 0.13
    [1] 1.7


# Tibbles


## Tibble Basics

Tibbles are enabled by the `tidyverse` library. They are similar to data frames but more sophisticated.

```R
library(tidyverse)
```

We will import sample data from a package. It will return a tibble data type.

```R
data(diamonds, package='ggplot2')
head(diamonds, 3)
```

<div class="org" id="org3b695d7">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">carat</th>
<th scope="col" class="org-left">cut</th>
<th scope="col" class="org-left">color</th>
<th scope="col" class="org-left">clarity</th>
<th scope="col" class="org-right">depth</th>
<th scope="col" class="org-right">table</th>
<th scope="col" class="org-right">price</th>
<th scope="col" class="org-right">x</th>
<th scope="col" class="org-right">y</th>
<th scope="col" class="org-right">z</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">0.23</td>
<td class="org-left">Ideal</td>
<td class="org-left">E</td>
<td class="org-left">SI2</td>
<td class="org-right">61.5</td>
<td class="org-right">55</td>
<td class="org-right">326</td>
<td class="org-right">3.95</td>
<td class="org-right">3.98</td>
<td class="org-right">2.43</td>
</tr>


<tr>
<td class="org-right">0.21</td>
<td class="org-left">Premium</td>
<td class="org-left">E</td>
<td class="org-left">SI1</td>
<td class="org-right">59.8</td>
<td class="org-right">61</td>
<td class="org-right">326</td>
<td class="org-right">3.89</td>
<td class="org-right">3.84</td>
<td class="org-right">2.31</td>
</tr>


<tr>
<td class="org-right">0.23</td>
<td class="org-left">Good</td>
<td class="org-left">E</td>
<td class="org-left">VS1</td>
<td class="org-right">56.9</td>
<td class="org-right">65</td>
<td class="org-right">327</td>
<td class="org-right">4.05</td>
<td class="org-right">4.07</td>
<td class="org-right">2.31</td>
</tr>
</tbody>
</table>

</div>

We can select data with `slice`.

```R
slice(diamonds, 3)
slice(diamonds, 1:7)
slice(diamonds, c(1, 7))
```

<div class="org" id="orga06ac42">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">carat</th>
<th scope="col" class="org-left">cut</th>
<th scope="col" class="org-left">color</th>
<th scope="col" class="org-left">clarity</th>
<th scope="col" class="org-right">depth</th>
<th scope="col" class="org-right">table</th>
<th scope="col" class="org-right">price</th>
<th scope="col" class="org-right">x</th>
<th scope="col" class="org-right">y</th>
<th scope="col" class="org-right">z</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">0.23</td>
<td class="org-left">Ideal</td>
<td class="org-left">E</td>
<td class="org-left">SI2</td>
<td class="org-right">61.5</td>
<td class="org-right">55</td>
<td class="org-right">326</td>
<td class="org-right">3.95</td>
<td class="org-right">3.98</td>
<td class="org-right">2.43</td>
</tr>


<tr>
<td class="org-right">0.24</td>
<td class="org-left">Very Good</td>
<td class="org-left">I</td>
<td class="org-left">VVS1</td>
<td class="org-right">62.3</td>
<td class="org-right">57</td>
<td class="org-right">336</td>
<td class="org-right">3.95</td>
<td class="org-right">3.98</td>
<td class="org-right">2.47</td>
</tr>
</tbody>
</table>

</div>


## Filtering Data

We use `filter` and conditionals for filtering data similar to a dataframe or sql.

```R
f1 <- filter(diamonds, cut=='Ideal')
f2 <- filter(diamonds, (cut=='Ideal' & price > 500))
head(f2, 3)
```

<div class="org" id="org55f643e">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">carat</th>
<th scope="col" class="org-left">cut</th>
<th scope="col" class="org-left">color</th>
<th scope="col" class="org-left">clarity</th>
<th scope="col" class="org-right">depth</th>
<th scope="col" class="org-right">table</th>
<th scope="col" class="org-right">price</th>
<th scope="col" class="org-right">x</th>
<th scope="col" class="org-right">y</th>
<th scope="col" class="org-right">z</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">0.35</td>
<td class="org-left">Ideal</td>
<td class="org-left">I</td>
<td class="org-left">VS1</td>
<td class="org-right">60.9</td>
<td class="org-right">57</td>
<td class="org-right">552</td>
<td class="org-right">4.54</td>
<td class="org-right">4.59</td>
<td class="org-right">2.78</td>
</tr>


<tr>
<td class="org-right">0.3</td>
<td class="org-left">Ideal</td>
<td class="org-left">D</td>
<td class="org-left">SI1</td>
<td class="org-right">62.5</td>
<td class="org-right">57</td>
<td class="org-right">552</td>
<td class="org-right">4.29</td>
<td class="org-right">4.32</td>
<td class="org-right">2.69</td>
</tr>


<tr>
<td class="org-right">0.3</td>
<td class="org-left">Ideal</td>
<td class="org-left">D</td>
<td class="org-left">SI1</td>
<td class="org-right">62.1</td>
<td class="org-right">56</td>
<td class="org-right">552</td>
<td class="org-right">4.3</td>
<td class="org-right">4.33</td>
<td class="org-right">2.68</td>
</tr>
</tbody>
</table>

</div>

```R
r1 <- diamonds %>% filter((cut=='Ideal' & price > 500)) %>% select(carat, price)
head(r1, 5)
```

<div class="org" id="org14ad407">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">carat</th>
<th scope="col" class="org-right">price</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">0.35</td>
<td class="org-right">552</td>
</tr>


<tr>
<td class="org-right">0.3</td>
<td class="org-right">552</td>
</tr>


<tr>
<td class="org-right">0.3</td>
<td class="org-right">552</td>
</tr>


<tr>
<td class="org-right">0.28</td>
<td class="org-right">553</td>
</tr>


<tr>
<td class="org-right">0.32</td>
<td class="org-right">553</td>
</tr>
</tbody>
</table>

</div>


## Mutate and Summary

Mutate adds new data to a column, we can name the columns with the variable we use for the operation.

```R
totalVol <- mutate(diamonds, totalVolume=(x*y*z))
head(totalVol, 3)
```

<div class="org" id="org44959aa">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">carat</th>
<th scope="col" class="org-left">cut</th>
<th scope="col" class="org-left">color</th>
<th scope="col" class="org-left">clarity</th>
<th scope="col" class="org-right">depth</th>
<th scope="col" class="org-right">table</th>
<th scope="col" class="org-right">price</th>
<th scope="col" class="org-right">x</th>
<th scope="col" class="org-right">y</th>
<th scope="col" class="org-right">z</th>
<th scope="col" class="org-right">totalVolume</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">0.23</td>
<td class="org-left">Ideal</td>
<td class="org-left">E</td>
<td class="org-left">SI2</td>
<td class="org-right">61.5</td>
<td class="org-right">55</td>
<td class="org-right">326</td>
<td class="org-right">3.95</td>
<td class="org-right">3.98</td>
<td class="org-right">2.43</td>
<td class="org-right">38.20203</td>
</tr>


<tr>
<td class="org-right">0.21</td>
<td class="org-left">Premium</td>
<td class="org-left">E</td>
<td class="org-left">SI1</td>
<td class="org-right">59.8</td>
<td class="org-right">61</td>
<td class="org-right">326</td>
<td class="org-right">3.89</td>
<td class="org-right">3.84</td>
<td class="org-right">2.31</td>
<td class="org-right">34.505856</td>
</tr>


<tr>
<td class="org-right">0.23</td>
<td class="org-left">Good</td>
<td class="org-left">E</td>
<td class="org-left">VS1</td>
<td class="org-right">56.9</td>
<td class="org-right">65</td>
<td class="org-right">327</td>
<td class="org-right">4.05</td>
<td class="org-right">4.07</td>
<td class="org-right">2.31</td>
<td class="org-right">38.076885</td>
</tr>
</tbody>
</table>

</div>

Getting summary values.

```R
s <- summarize(diamonds, mean(price))
head(s, 3)
```

<div class="org" id="org4cbcd20">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">mean(price)</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">3932.79972191324</td>
</tr>
</tbody>
</table>

</div>


## Grouping Data

Grouping data as summary.

```R
cut <- group_by(diamonds, cut)
s <- summarize(cut, mean(price), sum(price))
head(s, 3)
```

<div class="org" id="org606a699">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">cut</th>
<th scope="col" class="org-right">mean(price)</th>
<th scope="col" class="org-right">sum(price)</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">Fair</td>
<td class="org-right">4358.75776397516</td>
<td class="org-right">7017600</td>
</tr>


<tr>
<td class="org-left">Good</td>
<td class="org-right">3928.86445169181</td>
<td class="org-right">19275009</td>
</tr>


<tr>
<td class="org-left">Very Good</td>
<td class="org-right">3981.75989074657</td>
<td class="org-right">48107623</td>
</tr>
</tbody>
</table>

</div>

```R
s <- summarize(cut, avg=mean(price), num=n())
head(s, 3)
```

<div class="org" id="org2d4fc3e">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">cut</th>
<th scope="col" class="org-right">avg</th>
<th scope="col" class="org-right">num</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">Fair</td>
<td class="org-right">4358.75776397516</td>
<td class="org-right">1610</td>
</tr>


<tr>
<td class="org-left">Good</td>
<td class="org-right">3928.86445169181</td>
<td class="org-right">4906</td>
</tr>


<tr>
<td class="org-left">Very Good</td>
<td class="org-right">3981.75989074657</td>
<td class="org-right">12082</td>
</tr>
</tbody>
</table>

</div>

The summarization of a group by is a tibble.

```R
cut2 <- group_by(diamonds, cut, color)
cut2_sum <- summarize(cut2, mean(price))
head(cut2_sum, 3)
```

<div class="org" id="org289efeb">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">cut</th>
<th scope="col" class="org-left">color</th>
<th scope="col" class="org-right">mean(price)</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">Fair</td>
<td class="org-left">D</td>
<td class="org-right">4291.06134969325</td>
</tr>


<tr>
<td class="org-left">Fair</td>
<td class="org-left">E</td>
<td class="org-right">3682.3125</td>
</tr>


<tr>
<td class="org-left">Fair</td>
<td class="org-left">F</td>
<td class="org-right">3827.00320512821</td>
</tr>
</tbody>
</table>

</div>

We can use the group by as a tibble.

```R
c <- count(diamonds, cut)
head(c, 3)
```

<div class="org" id="org93db532">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">cut</th>
<th scope="col" class="org-right">n</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">Fair</td>
<td class="org-right">1610</td>
</tr>


<tr>
<td class="org-left">Good</td>
<td class="org-right">4906</td>
</tr>


<tr>
<td class="org-left">Very Good</td>
<td class="org-right">12082</td>
</tr>
</tbody>
</table>

</div>


# Reading Files with R


## Working with diectories

We can get the working directory, change it, list files and create files.

```R
getwd()
list.files()
setwd("src")
list.files()
setwd("..")
file.exists("README.md")
file.create("deleteme.R")
file.info("deleteme.R")
file.remove("deleteme.R")
```

    [1] "/Users/albertovaldez/data-notes/resources/week-15"
    [1] "LICENSE"   "Makefile"  "README.md" "config"    "docs"      "public"
    [7] "resources" "src"       "tests"
    [1] "rstats.org"
    [1] TRUE
    [1] TRUE
               size isdir mode               mtime               ctime
    deleteme.R    0 FALSE  644 2022-10-06 20:39:03 2022-10-06 20:39:03
                             atime uid gid         uname grname
    deleteme.R 2022-10-06 20:39:03 501  20 albertovaldez  staff
    [1] TRUE


## Reading CSV files

```R
demo_table <- read.csv(file='resources/demo.csv',check.names=F,stringsAsFactors = F)
demo_table
```

<div class="org" id="org045a7b2">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<tbody>
<tr>
<td class="org-left">John</td>
<td class="org-left">Compact_Sedan</td>
<td class="org-right">2012</td>
<td class="org-right">87456</td>
</tr>


<tr>
<td class="org-left">Claire</td>
<td class="org-left">Pickup</td>
<td class="org-right">2017</td>
<td class="org-right">15022</td>
</tr>


<tr>
<td class="org-left">Xavier</td>
<td class="org-left">SUV</td>
<td class="org-right">2019</td>
<td class="org-right">4532</td>
</tr>


<tr>
<td class="org-left">Kerri</td>
<td class="org-left">Subcompact_Sedan</td>
<td class="org-right">2018</td>
<td class="org-right">12349</td>
</tr>
</tbody>
</table>

</div>


## Reading JSON files

```R
library(jsonlite)
```

```R
demo_table2 <- fromJSON(txt='resources/demo.json')
head(demo_table2, 2)
```


# Hypothesis Testing


## Exploratory Data Analysis

In data science we are constantly manipulating, visualizng and summarizing data. This process is known as **exploratory data analysis**, which is part of the scientific method. It tells us how the data behaves, what happens in the past and what is happening right now. The complete scientific process would look like this:

Exploratory Analysis -> Hypothesis -> Experiment -> Hypothesis Testing -> Hypothesis Supported | Hypothesis Not Supported -> Report Results


## Hypothesis

A hypothesis is a proposed solution or answer to a question that we ask the data. We must ask specific questions that relate to the data contents and we know the data is be able to answer.

For example: Does this new medication help patients recover faster compared to a placebo group?

The hypothesis must have a specific structure so we can build an analysis around it. The outcome is something we can measure and compare, in this case recovery time over placebo patients.

**IF** we give patients the new drug -> **THEN** -> they recover 50% faster.

More examples:

**Question**: Is the cost of living in my city higher than another one?

**Hypothesis**: If we collect metrics around cots of living and total them together, is the average cost of living in my city is higher than the other city? In what percentage?

**Question**: Can we predict a IMDB score based in the score of the movie?

**Hypothesis**: Is there a linear relationship between the length of the movie and the IMDB score, if there is, the slope of the line should be nonzero.

The more specific, the easier the Hypothesis it is to test. However, the more specific a question is, the less meaningful it becomes, so we need to find a balance between generalization and testeability.


## Testing the Hypothesis

Once we have collected or generated the data, we can test it with a given expectation.

Null Hypothesis typically states that there is no difference between the variables of interests. There is nothing to look at the data, everything is the same.

&ldquo;Having this observation doesn&rsquo;t mean that another one will be related&rdquo;.

Alternative Hypothesis is what we will reflect our guess upon, which is closer to our actual Hypothesis. It is a way to negate the Null Hyphotesis.

&ldquo;If we have this observation, then this should be true&rdquo;.

Our test consists on negating the Null Hypothesis, that it can be rejected. However, we will never reject the Null Hypothesis absolutely.

The `p-value` (probability value) is the chance of the Null Hypothesis to occurr. Having a result at least as extreme as what was previously observed.

If we have a low enough `p-value`, we can reject the Null Hypothesis. The `confidence` is the threshold that we can accept for not negating the Null Hypothesis.

The significance of the test is the probability that we are wrong. The confidence limit is normally `95%`, so we will aim for significances of 5%, 2.5% or 1%. If the `p-value` is lower that the significance, we can reject the Null Hypothesis.

1.  Determine the Hypothesis and Null Hypothesis.
2.  Identify appropiate statistical test.
3.  Determine acceptable significance value.
4.  Compute p-value.
5.  Determine if p-value rejects the null hypothesis.


## Types of Tests

The t-test is a way to compare the mean of a sample against a population or another sample. The way we compute one-sample t-test is different from two-sample t-test.

We are able to compare a sample to a population taken from the same sample. The population&rsquo;s metrics are computed differently to samples metrics, as the later are estimated metrics.

The Null Hypothesis assumes that there are no meaningful differences between the two means. The goal of the t-test is to reject the Null Hypothesis.

A Null Hypothesis would be &ldquo;if we compare the mean of a sample to the population&rsquo;s mean, there would be no difference&rdquo;, so we use the t-test for this case.

The result of a t-test is a test statistic. We can convert the test result to a p-value by building a member of the distribution and we will measure the probability of that measure to happen, which is the p-value.

For example: we have a medication we are testing, and we are comparing it to another product, where our hypothesis is that it is more effective than the other product.


# Hypothesis Testing in R


## One-Sample T-Test

We can construct a `t-test` in R by creating a sample of the population and then using `t.test` to compare them.

In the first case, the `p-value` is very high so we cannot reject the Null Hypothesis.

```R
set.seed(42)
population1 = rnorm(1000)
population2 = sample(population1, 200)
t.test(population2, mu=mean(population1))
```

    
    	One Sample t-test
    
    data:  population2
    t = 0.67172, df = 199, p-value = 0.5025
    alternative hypothesis: true mean is not equal to -0.02582443
    95 percent confidence interval:
     -0.1222976  0.1703285
    sample estimates:
     mean of x
    0.02401546

In this case, with a different population, the `p-value` is very low, so we can reject the Null Hypothesis.

```R
population3 = rnorm(1000, -2)
t.test(population2, mu=mean(population3))
```

    
    	One Sample t-test
    
    data:  population2
    t = 27.231, df = 199, p-value < 2.2e-16
    alternative hypothesis: true mean is not equal to -1.996426
    95 percent confidence interval:
     -0.1222976  0.1703285
    sample estimates:
     mean of x
    0.02401546


## Two-Sample T-Test

In our first test, we are comparing two samples, so the test is applied differently.

```R
set.seed(42)
population1 = rnorm(1000)
population2 = rnorm(1000)
t.test(population1, population2)
```

    
    	Welch Two Sample t-test
    
    data:  population1 and population2
    t = -0.46115, df = 1997.5, p-value = 0.6447
    alternative hypothesis: true difference in means is not equal to 0
    95 percent confidence interval:
     -0.10771412  0.06670125
    sample estimates:
       mean of x    mean of y
    -0.025824427 -0.005317994

In the second case, we are comparing a new sample to our original sample and the result is a very low `p-value`.

```R
population1 = rnorm(1000)
population2 = rnorm(1000, -2)
t.test(population1, population2)
```

    
    	Welch Two Sample t-test
    
    data:  population1 and population2
    t = 44.698, df = 1994.9, p-value < 2.2e-16
    alternative hypothesis: true difference in means is not equal to 0
    95 percent confidence interval:
     1.930014 2.107146
    sample estimates:
       mean of x    mean of y
    -0.003206987 -2.021786821


## T.Test Example in R

```shell
cat ../resources/03/Resources/description.md
```

    # Dataset: sardine.dat
    
    Source: F.N. Clark (1936). "Variations in the Number of Vertebrae in Sardine,
    Sardinops caerulea (Girard)", Copeia, Vol. 1936, #3, pp.147-150.
    
    Description: Number of Vertebrae and Location of 12858 adult sardines caught
    in the Pacific.
    
    Locations:
    1=Alaska
    2=British Columbia
    3=San Francisco
    4=Monterey
    5=San Pedro
    6=San Diego
    
    Variables/Columns
    Location   8
    Number of Vertebrae   15-16

```R
sardines <- read.csv(file="./resources/03/Resources/sardines.csv")
# Calculate the population mean for Sardine Vertebrae in Alaska.
# Hint: use the subset() function to get only the data for Alaska.
alaska <- sardines %>% subset(location == 1)
mean(alaska[['vertebrae']])
# Calculate the population mean for Sardine Vertebrae in San Diego.
# Hint: use the subset() function to get only the data for San Diego.
sandiego <- sardines %>% subset(location == 6)
mean(sandiego[['vertebrae']])
# Calculate Independent (Two Sample) T-Test
t.test(alaska[['vertebrae']], sandiego[['vertebrae']])
```

    [1] 51.5
    [1] 51.74336
    
    	Welch Two Sample t-test
    
    data:  alaska[["vertebrae"]] and sandiego[["vertebrae"]]
    t = -1.9607, df = 25.984, p-value = 0.06072
    alternative hypothesis: true difference in means is not equal to 0
    95 percent confidence interval:
     -0.4985070  0.0117813
    sample estimates:
    mean of x mean of y
     51.50000  51.74336


# ANOVA Test

If we want to compare multiple samples and multiple characteristics, we can use Analysis of Variance (ANOVA). It compares more than two sample groups.

The Alternate Hypothesis is that at least the mean of one population is different, where the Null Hypothesis says that there is no difference between any of the means.

ANOVA compares the means across all samples and determines wether there is a significant difference in at least one sample.

It assumes that the standard deviation is the same among all groups, which means that all samples come from the same population. If that&rsquo;s not true, the result may we wrong.


## ANOVA Test in R

```shell
cat ../resources/05/Resources/description.md
```

    # Description
    
    Studies conducted at the University of Melbourne indicate that there may be a difference between the pain thresholds of blonds and brunettes. Men and women of various ages were divided into four categories according to hair colour: light blond, dark blond, light brunette, and dark brunette. The purpose of the experiment was to determine whether hair colour is related to the amount of pain produced by common types of mishaps and assorted types of trauma. Each person in the experiment was given a pain threshold score based on his or her performance in a pain sensitivity test (the higher the score, the higher the person’s pain tolerance).
    
    Variable      Values
    HairColour    LightBlond, DarkBlond, LightBrunette or DarkBrunette
    Pain          Pain threshold score
    Download
    Data file (tab-delimited text)
    
    Source
    Family Weekly, Gainesville, Sun, Gainesville, Florida, February 5, 1978.
    
    McClave, J. T., and Dietrich II, F. H. (1991). Statistics. Dellen Publishing, San Francisco, Exercise 10.20.
    Analysis
    Pain threshold decreases as hair darkness increases (blonds are tougher!). Light blonds are significantly different from both brunette categories. Other contrasts are not significant.

```R
library(ggplot2)
```

```R
hair <- read.csv(file="../resources/05/Resources/hair.csv")
head(hair, 5)
```

      HairColour Pain
    1 LightBlond   62
    2 LightBlond   60
    3 LightBlond   71
    4 LightBlond   55
    5 LightBlond   48

```R
#  Plot the data using ggplot
ggplot(hair, aes(x=HairColour, y=Pain)) + geom_boxplot()
```

<div class="org" id="org69c57f5">

<div id="org1f6bc13" class="figure">
<p><img src="./../resources/hair.png" alt="hair.png" />
</p>
</div>

</div>

```R
# Determine the p-value using ANOVA
summary(aov(Pain ~ HairColour, data=hair))
```

                Df Sum Sq Mean Sq F value  Pr(>F)
    HairColour   3   1361   453.6   6.791 0.00411 **
    Residuals   15   1002    66.8
    ---
    Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1


# Linear Relationship

The equation of a line is `y = ax + b`. Which means we can determine the value of `y` based on `y`.

The `b` will offset the `y` values by a fix amount and the `a` will modify the slope of the curve.

```R
diabetes <- read.csv(file="../resources/06/Resources/diabetes.csv")
head(diabetes, 5)
```

<div class="org" id="org7a28485">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">X</th>
<th scope="col" class="org-right">age</th>
<th scope="col" class="org-right">sex</th>
<th scope="col" class="org-right">bmi</th>
<th scope="col" class="org-right">bp</th>
<th scope="col" class="org-right">s1</th>
<th scope="col" class="org-right">s2</th>
<th scope="col" class="org-right">s3</th>
<th scope="col" class="org-right">s4</th>
<th scope="col" class="org-right">s5</th>
<th scope="col" class="org-right">s6</th>
<th scope="col" class="org-right">One_Year_Disease_Progress</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">0</td>
<td class="org-right">0.038075906</td>
<td class="org-right">0.050680119</td>
<td class="org-right">0.061696207</td>
<td class="org-right">0.021872355</td>
<td class="org-right">-0.044223498</td>
<td class="org-right">-0.034820763</td>
<td class="org-right">-0.043400846</td>
<td class="org-right">-0.002592262</td>
<td class="org-right">0.019908421</td>
<td class="org-right">-0.017646125</td>
<td class="org-right">151</td>
</tr>


<tr>
<td class="org-right">1</td>
<td class="org-right">-0.001882017</td>
<td class="org-right">-0.044641637</td>
<td class="org-right">-0.051474061</td>
<td class="org-right">-0.026327835</td>
<td class="org-right">-0.008448724</td>
<td class="org-right">-0.01916334</td>
<td class="org-right">0.074411564</td>
<td class="org-right">-0.039493383</td>
<td class="org-right">-0.068329744</td>
<td class="org-right">-0.09220405</td>
<td class="org-right">75</td>
</tr>


<tr>
<td class="org-right">2</td>
<td class="org-right">0.085298906</td>
<td class="org-right">0.050680119</td>
<td class="org-right">0.044451213</td>
<td class="org-right">-0.005670611</td>
<td class="org-right">-0.045599451</td>
<td class="org-right">-0.034194466</td>
<td class="org-right">-0.032355932</td>
<td class="org-right">-0.002592262</td>
<td class="org-right">0.002863771</td>
<td class="org-right">-0.025930339</td>
<td class="org-right">141</td>
</tr>


<tr>
<td class="org-right">3</td>
<td class="org-right">-0.089062939</td>
<td class="org-right">-0.044641637</td>
<td class="org-right">-0.011595015</td>
<td class="org-right">-0.036656447</td>
<td class="org-right">0.012190569</td>
<td class="org-right">0.024990593</td>
<td class="org-right">-0.03603757</td>
<td class="org-right">0.034308859</td>
<td class="org-right">0.022692023</td>
<td class="org-right">-0.009361911</td>
<td class="org-right">206</td>
</tr>


<tr>
<td class="org-right">4</td>
<td class="org-right">0.00538306</td>
<td class="org-right">-0.044641637</td>
<td class="org-right">-0.036384692</td>
<td class="org-right">0.021872355</td>
<td class="org-right">0.003934852</td>
<td class="org-right">0.01559614</td>
<td class="org-right">0.008142084</td>
<td class="org-right">-0.002592262</td>
<td class="org-right">-0.031991445</td>
<td class="org-right">-0.046640874</td>
<td class="org-right">135</td>
</tr>
</tbody>
</table>

</div>

Non-clear linear relationship.

```R
ggplot(diabetes, aes(bp, One_Year_Disease_Progress)) + geom_point() + geom_smooth(method="lm", se=TRUE)
```

<div class="org" id="org1784d76">

<div id="orgfdad975" class="figure">
<p><img src="./../resources/diabetes.png" alt="diabetes.png" />
</p>
</div>

</div>

Building our own linear regretion. The `R-squared` is a coefficient of determination, it tells us which percentage of the variability affects the blood pressure. This gives us a linear relationship between two variables.

```R
reg <- lm(One_Year_Disease_Progress ~ bp, data=diabetes)
summary(reg)
```

    
    Call:
    lm(formula = One_Year_Disease_Progress ~ bp, data = diabetes)
    
    Residuals:
         Min       1Q   Median       3Q      Max
    -173.903  -53.122   -7.811   52.057  227.449
    
    Coefficients:
                Estimate Std. Error t value Pr(>|t|)
    (Intercept)  152.133      3.294   46.19   <2e-16 ***
    bp           714.742     69.252   10.32   <2e-16 ***
    ---
    Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
    
    Residual standard error: 69.25 on 440 degrees of freedom
    Multiple R-squared:  0.1949,	Adjusted R-squared:  0.1931
    F-statistic: 106.5 on 1 and 440 DF,  p-value: < 2.2e-16

The number we are interested in is **Adjusted R-squared: 0.1931**.

## Footnotes

<sup><a id="fn.1" class="footnum" href="#fnr.1">1</a></sup> <https://www.rstudio.com/>

<sup><a id="fn.2" class="footnum" href="#fnr.2">2</a></sup> <https://support.rstudio.com/hc/en-us/articles/200711853-Keyboard-Shortcuts>

<sup><a id="fn.3" class="footnum" href="#fnr.3">3</a></sup> [R Notebook](https://uc-r.github.io/r_notebook#:~:text=An%20R%20Notebook%20is%20an,document%20to%20see%20the%20output)
